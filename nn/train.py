import sqlite3
import yaml
from MonoIgnoreDataSet import MonoIgnoreDataSet
from MonoTagDataSet import MonoTagDataSet
from MonoModel import MonoModel

conn = sqlite3.connect('../monobank.sqlite')
cursor = conn.cursor()
tagsRaw = [row[0] for row in cursor.execute('SELECT DISTINCT(tags) FROM statements WHERE for_training = 1').fetchall()]
tags = []
for tagRaw in tagsRaw:
    if tagRaw is None:
        continue
    t = tagRaw.strip('|').split('|')
    tags = tags + t
tags = list(set(tags))
accounts = [row[0] for row in cursor.execute('SELECT id FROM accounts order by position').fetchall()]
descriptions = [row[0] for row in cursor.execute('SELECT DISTINCT(description) FROM statements WHERE account_id = "fa_tttbZiiB5EtN6WCtgEw" AND for_training = 1').fetchall()]
mccs = [row[0] for row in cursor.execute('SELECT DISTINCT(mcc) FROM statements WHERE account_id = "fa_tttbZiiB5EtN6WCtgEw" AND for_training = 1').fetchall()]
rows = cursor.execute('SELECT s.id, s.amount, s.ignore, s.tags, s.account_id, description, mcc FROM statements s WHERE account_id = "fa_tttbZiiB5EtN6WCtgEw" AND s.for_training = 1').fetchall()

dataset = MonoIgnoreDataSet(accounts, descriptions, mccs, rows)
dataset.raw_data_to_structured()
dataset.structured_data_to_flat_dataset()
print("Ignore")
model = MonoModel(dataset)
model.train()
accuracy = model.calc_accuracy()
accuracies = {}
print("Accuracy:", accuracy)
accuracies['ignore'] = accuracy
model.save('ignore')
for tag in tags:
    dataset = MonoTagDataSet(accounts, descriptions, mccs, rows, tag)
    dataset.raw_data_to_structured()
    dataset.structured_data_to_flat_dataset()
    print(tag)
    model = MonoModel(dataset)
    model.train()
    accuracy = model.calc_accuracy()
    print("Accuracy:", accuracy)
    model.save(tag)
    accuracies[tag] = accuracy

file_path = "./model/model.yaml"
with open(file_path, 'w') as file:
    yaml.dump(accuracies, file)

cursor.close()
conn.close()

# import matplotlib.pyplot as plt
# plt.plot(loss_arr)
# plt.show()
