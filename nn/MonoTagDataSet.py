import numpy as np
from AbstractMonoDataSet import AbstractMonoDataSet


class MonoTagDataSet(AbstractMonoDataSet):
    def __init__(self, accounts, descriptions, mccs, rows, tag):
        super().__init__(accounts, descriptions, mccs, rows)
        self.tag = tag


    def raw_data_to_structured(self):
        statements = []
        for row in self.rows:
            statement_tags = row[3]
            statements.append({
                'input': self.get_input(row),
                'target': 0 if statement_tags is None else int('|' + self.tag + '|' in statement_tags)
            })
        self.statements = statements

    def structured_data_to_flat_dataset(self):
        data = []
        target = []
        for statement in self.statements:
            statements_input = [statement['input']['positive_amount']] \
                               + statement['input']['accounts'] \
                               + statement['input']['descriptions'] \
                               + statement['input']['mccs']
            data.append(np.array(statements_input))
            target.append(statement['target'])
        result = {"data": np.array(data), "target": np.array(target)}
        self.flat_training_data = result
        self.training_dataset = [(result['data'][i][None, ...], result['target'][i]) for i in range(len(result['target']))]
