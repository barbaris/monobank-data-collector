import random
import numpy as np
from nn_utils import NnUtils


class MonoModel:
    def __init__(self, training_dataset):
        self.training_dataset = training_dataset

        self.W1 = None
        self.W2 = None
        self.b1 = None
        self.b2 = None


    def train(self):
        INPUT_DIM = self.training_dataset.get_input_dim()
        OUT_DIM = 2
        H_DIM = INPUT_DIM + 2

        ALPHA = 0.0002
        NUM_EPOCHS = 1000
        BATCH_SIZE = 50

        W1 = np.random.rand(INPUT_DIM, H_DIM)
        b1 = np.random.rand(1, H_DIM)
        W2 = np.random.rand(H_DIM, OUT_DIM)
        b2 = np.random.rand(1, OUT_DIM)

        W1 = (W1 - 0.5) * 2 * np.sqrt(1 / INPUT_DIM)
        b1 = (b1 - 0.5) * 2 * np.sqrt(1 / INPUT_DIM)
        W2 = (W2 - 0.5) * 2 * np.sqrt(1 / H_DIM)
        b2 = (b2 - 0.5) * 2 * np.sqrt(1 / H_DIM)

        loss_arr = []
        dataset = self.training_dataset.get_training_data()
        for ep in range(NUM_EPOCHS):
            random.shuffle(dataset)
            for i in range(len(dataset) // BATCH_SIZE):
                batch_x, batch_y = zip(*dataset[i * BATCH_SIZE: i * BATCH_SIZE + BATCH_SIZE])
                x = np.concatenate(batch_x, axis=0)
                y = np.array(batch_y)

                # Forward
                t1 = x @ W1 + b1
                h1 = NnUtils.relu(t1)
                t2 = h1 @ W2 + b2
                z = NnUtils.softmax_batch(t2)
                E = np.sum(NnUtils.sparse_cross_entropy_batch(z, y))

                # Backward
                y_full = NnUtils.to_full_batch(y, OUT_DIM)
                dE_dt2 = z - y_full
                dE_dW2 = h1.T @ dE_dt2
                dE_db2 = np.sum(dE_dt2, axis=0, keepdims=True)
                dE_dh1 = dE_dt2 @ W2.T
                dE_dt1 = dE_dh1 * NnUtils.relu_deriv(t1)
                dE_dW1 = x.T @ dE_dt1
                dE_db1 = np.sum(dE_dt1, axis=0, keepdims=True)

                # Update
                W1 = W1 - ALPHA * dE_dW1
                b1 = b1 - ALPHA * dE_db1
                W2 = W2 - ALPHA * dE_dW2
                b2 = b2 - ALPHA * dE_db2

                loss_arr.append(E)
        self.W1 = W1
        self.b1 = b1
        self.W2 = W2
        self.b2 = b2

    def predict(self, x):
        t1 = x @ self.W1 + self.b1
        h1 = NnUtils.relu(t1)
        t2 = h1 @ self.W2 + self.b2
        z = NnUtils.softmax_batch(t2)
        return z

    def calc_accuracy(self):
        correct = 0
        for x, y in self.training_dataset.get_training_data():
            z = self.predict(x)
            y_pred = np.argmax(z)
            if y_pred == y:
                correct += 1
        return correct / len(self.training_dataset.get_training_data())

    def save(self, filename):
        self.training_dataset.save('./model/' + filename + '.dataset')
        np.savez('./model/' + filename, W1=self.W1, W2=self.W2, b1=self.b1, b2=self.b2)

    def load(self, filename):
        data = np.load(filename)
        self.W1 = data['W1']
        self.W2 = data['W2']
        self.b1 = data['b1']
        self.b2 = data['b2']
