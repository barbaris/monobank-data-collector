from abc import ABC

import numpy as np

from AbstractTrainingDataSet import AbstractTrainingDataSet


class AbstractMonoDataSet(AbstractTrainingDataSet, ABC):
    def __init__(self, accounts, descriptions, mccs, rows):
        self.accounts = accounts
        self.descriptions = descriptions
        self.mccs = mccs
        self.rows = rows

        self.statements = None
        self.flat_training_data = None
        self.training_dataset = None

    def convert_to_x2(self, positive_amount, account_id, statement_description, statement_mcc):
        accounts_input = []
        for account in self.accounts:
            accounts_input.append(int(account_id == account))

        descriptions_input = []
        for description in self.descriptions:
            descriptions_input.append(int(description == statement_description))

        mccs_input = []
        for mcc in self.mccs:
            mccs_input.append(int(mcc == statement_mcc))

        x = np.array([positive_amount] + accounts_input + descriptions_input + mccs_input)
        return x

    def get_input(self, row):
        statement_id, statement_amount, statement_ignore, statement_tags, statement_account_id, statement_description, statement_mcc = row

        positive_amount = int(statement_amount > 0)

        accounts_input = []
        for account in self.accounts:
            accounts_input.append(int(statement_account_id == account))

        descriptions_input = []
        for description in self.descriptions:
            descriptions_input.append(int(statement_description == description))

        mccs_input = []
        for mcc in self.mccs:
            mccs_input.append(int(statement_mcc == mcc))

        return {
            'positive_amount': positive_amount,
            'accounts': accounts_input,
            'descriptions': descriptions_input,
            'mccs': mccs_input,
        }

    def convert_to_x(self, positive_amount, account_id, statement_description, statement_mcc):
        accounts_input = []
        for account in self.accounts:
            accounts_input.append(int(account_id == account))

        descriptions_input = []
        for description in self.descriptions:
            descriptions_input.append(int(description == statement_description))

        mccs_input = []
        for mcc in self.mccs:
            mccs_input.append(int(mcc == statement_mcc))

        x = np.array([positive_amount] + accounts_input + descriptions_input + mccs_input)
        return x

    def get_input_dim(self):
        return len(self.flat_training_data['data'][0])

    def get_training_data(self):
        return self.training_dataset

    def save(self, filename):
        np.savez(filename, accounts=self.accounts, descriptions=self.descriptions, mccs=self.mccs)

    def load(self, filename):
        data = np.load(filename)
        self.accounts = data['accounts']
        self.descriptions = data['descriptions']
        self.mccs = data['mccs']
