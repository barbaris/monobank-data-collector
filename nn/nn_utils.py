import numpy as np


class NnUtils:
    @staticmethod
    def relu(t):
        return np.maximum(t, 0)

    @staticmethod
    def softmax_batch(t):
        out = np.exp(t)
        return out / np.sum(out, axis=1, keepdims=True)

    @staticmethod
    def sparse_cross_entropy_batch(z, y):
        return -np.log(np.array([z[j, y[j]] for j in range(len(y))]))

    @staticmethod
    def to_full(y, num_classes):
        y_full = np.zeros((1, num_classes))
        y_full[0, y] = 1
        return y_full

    @staticmethod
    def to_full_batch(y, num_classes):
        y_full = np.zeros((len(y), num_classes))
        for j, yj in enumerate(y):
            y_full[j, yj] = 1
        return y_full

    @staticmethod
    def relu_deriv(t):
        return (t >= 0).astype(float)
