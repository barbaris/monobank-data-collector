import numpy as np
from AbstractMonoDataSet import AbstractMonoDataSet


class MonoIgnoreDataSet(AbstractMonoDataSet):
    def __init__(self, accounts, descriptions, mccs, rows):
        super().__init__(accounts, descriptions, mccs, rows)

    def raw_data_to_structured(self):
        statements = []
        for row in self.rows:
            statement_ignore = row[2]

            statements.append({
                'input': self.get_input(row),
                'target': statement_ignore
            })
        self.statements = statements

    def structured_data_to_flat_dataset(self):
        data = []
        target = []
        for statement in self.statements:
            statements_input = [statement['input']['positive_amount']] \
                               + statement['input']['accounts'] \
                               + statement['input']['mccs'] \
                               + statement['input']['descriptions']
            data.append(np.array(statements_input))
            target.append(statement['target'])
        result = {"data": np.array(data), "target": np.array(target)}
        self.flat_training_data = result
        self.training_dataset = [(result['data'][i][None, ...], result['target'][i]) for i in range(len(result['target']))]
