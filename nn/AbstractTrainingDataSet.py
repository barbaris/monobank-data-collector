from abc import ABC, abstractmethod


class AbstractTrainingDataSet(ABC):
    @abstractmethod
    def get_training_data(self):
        pass

    @abstractmethod
    def get_input_dim(self):
        pass
