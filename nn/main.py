import sqlite3

import numpy as np
import yaml

from MonoIgnoreDataSet import MonoIgnoreDataSet
from MonoTagDataSet import MonoTagDataSet
from MonoModel import MonoModel

conn = sqlite3.connect('../monobank.sqlite')
cursor = conn.cursor()
file_path = "./model/model.yaml"
with open(file_path, 'r') as file:
    loaded_dict = yaml.safe_load(file)
    tags = list(loaded_dict.keys())
dataset = MonoIgnoreDataSet(None, None, None, None)
dataset.load('./model/ignore.dataset.npz')
print("Ignore")
model = MonoModel(dataset)
model.load('./model/ignore.npz')

rows_to_process = cursor.execute('SELECT s.id, s.amount, s.account_id, description, mcc FROM statements s WHERE account_id = "fa_tttbZiiB5EtN6WCtgEw" AND s.time > 1691689587 AND nn_processed = 0').fetchall()

for row in rows_to_process:
    statement_id, statement_amount, statement_account_id, statement_description, statement_mcc = row
    result = model.predict(dataset.convert_to_x(int(statement_amount > 0), statement_account_id, statement_description, statement_mcc))
    print(row, result, np.argmax(result))
    if np.argmax(result) == 1:
        print("Ignore", statement_description, statement_amount / 100)
        cursor.execute('UPDATE statements SET ignore = 1 WHERE id = :id', {'id': row[0]})
        conn.commit()

for tag in tags:
    print("Tag", tag)
    dataset = MonoTagDataSet(None, None, None, None, tag)
    dataset.load('./model/' + tag + '.dataset.npz')

    model = MonoModel(dataset)
    model.load('./model/' + tag + '.npz')
    rows_to_process = cursor.execute('SELECT s.id, s.amount, s.account_id, description, mcc FROM statements s WHERE account_id = "fa_tttbZiiB5EtN6WCtgEw" AND s.time > 1691689587 AND nn_processed = 0').fetchall()

    for row in rows_to_process:
        statement_id, statement_amount, statement_account_id, statement_description, statement_mcc = row
        result = model.predict(dataset.convert_to_x(int(statement_amount > 0), statement_account_id, statement_description, statement_mcc))
        print(np.argmax(result), row, result)
        if np.argmax(result) == 1:
            cursor.execute('SELECT tags FROM statements WHERE id = :id', {'id': row[0]})
            print("Tag", tag, statement_description, statement_amount / 100)
            tags = cursor.fetchone()[0]
            if tags is None:
                tags = '|'
            if '|'+tag+'|' not in tags:
                tags += tag+'|'
                cursor.execute('UPDATE statements SET tags = :tags WHERE id = :id', {'id': row[0], 'tags': tags})
                conn.commit()

cursor.execute('UPDATE statements SET nn_processed = 1 WHERE nn_processed = 0')
conn.commit()

cursor.close()
conn.close()
