/*
    SPDX-FileCopyrightText: 2023 Oleksandr Borys <barbaris.in@gmail.com>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/

import QtQuick 2.15
import QtQuick.Layouts 1.1
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.kcoreaddons 1.0 as KCoreAddons

ListView {
    id: main
    Plasmoid.preferredRepresentation: Plasmoid.fullRepresentation
    model: listModel
    property string fileUrl: "file:///home/barbaris/.cache/mono.txt";

    ListModel {
        id: listModel
        ListElement { value: 0 }
        ListElement { value: 0 }
        ListElement { value: 0 }
        ListElement { value: 0 }
    }

    delegate: Item {
        width: parent.width
        height: 30
        Text {
            anchors.right: parent.right
            text: model.value.toLocaleString(Qt.locale(), "f", 0) + " "
            color: model.value > 0 ? "white" : "#dd0000"
            font.pixelSize: 24
            horizontalAlignment: Text.AlignRight
        }
    }

    Component.onCompleted: {
        refresh()
    }

    function getDataFromUrl(url, callback) {
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    callback(xhr.responseText);
                } else {
                    console.error("Failed to fetch data:", xhr.statusText);
                }
            }
        };
        xhr.open("GET", url);
        xhr.send();
    }

    PlasmaCore.DataSource {
        id: executable
        engine: "executable"
        connectedSources: []
        onNewData: {
            var exitCode = data["exit code"]
            var exitStatus = data["exit status"]
            var stdout = data["stdout"]
            var stderr = data["stderr"]
            exited(sourceName, exitCode, exitStatus, stdout, stderr)
            disconnectSource(sourceName)
            console.log(stdout, stderr)
        }
        function exec(cmd) {
            if (cmd) {
                connectSource(cmd)
            }
        }
        signal exited(string cmd, int exitCode, int exitStatus, string stdout, string stderr)
    }

    function refresh() {
        console.log("Refreshing...")
        executable.exec("cd /home/barbaris/Projects/monobank-data-collector && node src/report.js")
        getDataFromUrl(fileUrl, function(data) {
            console.log(data)
            var lines = data.split("\n")
            for (var i = 0; i < lines.length; i++) {
                listModel.setProperty(i, "value", parseInt(lines[i]))
            }
        })
    }

    Timer {
        interval: 1000*60*5
        repeat: true
        running: true
        onTriggered: refresh()
    }

    Timer {
        interval: 1000*60*60
        repeat: true
        running: true
        onTriggered: {
            executable.exec("cd /home/barbaris/Projects/monobank-data-collector && node src/import.js && cd /home/barbaris/Projects/monobank-data-collector/nn && /home/barbaris/Projects/monobank-data-collector/nn/venv/bin/python /home/barbaris/Projects/monobank-data-collector/nn/main.py")
        }
    }
}
