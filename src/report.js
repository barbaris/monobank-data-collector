require('dotenv').config();
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./monobank.sqlite');

const getMonthRange = (date) => {
    const from = new Date(date.getTime());
    from.setUTCDate(1);
    from.setUTCHours(0, 0, 0, 0);
    const to = new Date(date.getTime());
    to.setUTCFullYear(to.getUTCFullYear(), to.getUTCMonth() + 1, 1);
    to.setUTCHours(0, 0, 0, 0);
    const fromTimestamp = Math.floor(from.getTime() / 1000);
    const toTimestamp = Math.floor(to.getTime() / 1000);
    return {fromTimestamp, toTimestamp};
}

const getMonthReport = (date) => {
    const {fromTimestamp, toTimestamp} = getMonthRange(date);
    const music = new Promise((resolve, reject) => {
        db.get('select sum(amount)/100 as music' +
            '      from statements\n' +
            '      where account_id = \'fa_tttbZiiB5EtN6WCtgEw\'\n' +
            '        and time >= ' + fromTimestamp + '\n' +
            '        and time < ' + toTimestamp + '\n' +
            '        and tags LIKE "%|music|%"\n' +
            '      ', (err, row) => {
            if (err) reject(err);
            resolve(row);
        });
    });
    const utilities = new Promise((resolve, reject) => {
        db.get('select sum(amount)/100  as utilities' +
            '      from statements\n' +
            '      where account_id = \'fa_tttbZiiB5EtN6WCtgEw\'\n' +
            '        and time >= ' + fromTimestamp + '\n' +
            '        and time < ' + toTimestamp + '\n' +
            '        and tags LIKE "%|utilities|%"\n' +
            '      ', (err, row) => {
            if (err) reject(err);
            resolve(row);
        });
    });
    const everyday = new Promise((resolve, reject) => {
        db.get('select sum(amount)/100  as everyday' +
            '      from statements\n' +
            '      where account_id = \'fa_tttbZiiB5EtN6WCtgEw\'\n' +
            '        and time >= ' + fromTimestamp + '\n' +
            '        and time < ' + toTimestamp + '\n' +
            '        and ignore = 0\n' +
            '      ', (err, row) => {
            if (err) reject(err);
            resolve(row);
        });
    });

    return Promise.all([everyday, music, utilities]);
}

const getWeekends = (currentDate) => {
    const startOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
    let saturdaysCount = 0;
    let sundaysCount = 0;
    for (let date = startOfMonth; date <= currentDate; date.setDate(date.getDate() + 1)) {
        if (date.getDay() === 6) {
            saturdaysCount++;
        } else if (date.getDay() === 0) {
            sundaysCount++;
        }
    }

    return saturdaysCount + sundaysCount;
}

const getDays = (date) => {
    let {fromTimestamp, toTimestamp} = getMonthRange(date);
    const now = (new Date()).getTime() / 1000;
    if (now < toTimestamp) {
        toTimestamp = now;
    }

    return (toTimestamp - fromTimestamp) / 86400 + getWeekends(date);
}

const getPlanPerDay = (date) => {
    const days = getDays(date);
    return Math.round(1500 * days);
}

const getFactPerDay = (date) => {
    return new Promise((resolve, reject) => {
        let {fromTimestamp, toTimestamp} = getMonthRange(date);

        db.get('SELECT -SUM(amount) / 100 as plan_per_day FROM statements WHERE account_id = \'fa_tttbZiiB5EtN6WCtgEw\'\n' +
            '        AND time >= ' + fromTimestamp + '\n' +
            '        AND time < ' + toTimestamp + '\n' +
            '        AND ignore = 0', (err, row) => {
            if (err) reject(err);
            resolve(row.plan_per_day);
        });
    });
}

(async () => {
    const d = new Date();
    const total_fact = await getFactPerDay(d);
    const total_plan = getPlanPerDay(d);

    const report = {
        fact_per_day: Math.round(total_fact / getDays(d)),
        total_fact: total_fact,
        total_plan: total_plan,
        proficit: total_plan - total_fact
    }

    console.log(report);
    const fs = require('fs');
    fs.writeFile('/home/barbaris/.cache/mono.txt', report.fact_per_day.toString() + "\n" + report.total_fact.toString() + "\n" + report.total_plan.toString() + "\n" + report.proficit.toString(), function (err) {
        if (err) throw err;
    });
    console.log('Report created');
})();
