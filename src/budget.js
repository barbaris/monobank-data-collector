const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./monobank.sqlite');
const table = require('table');
const short = false;

const data = [['Бюджет на жовтень 2023', '', '', '', '']];
const budgetId = 1;

function f(val) {
    return val.toLocaleString('uk-UA');
}

const m = new Date('2023-10-15');

const getMonthRange = (date) => {
    const from = new Date(date.getTime());
    from.setUTCDate(1);
    from.setUTCHours(0, 0, 0, 0);
    const to = new Date(date.getTime());
    to.setUTCFullYear(to.getUTCFullYear(), to.getUTCMonth() + 1, 1);
    to.setUTCHours(0, 0, 0, 0);
    const fromTimestamp = Math.floor(from.getTime() / 1000);
    const toTimestamp = Math.floor(to.getTime() / 1000);
    return {fromTimestamp, toTimestamp};
}

const getBalances = (budgetId) => {
    return new Promise((resolve, reject) => {
        db.all('SELECT name, currency_amount, currency_rate, currency, round(currency_rate * currency_amount) as amount FROM balances WHERE budget_id = ?', [budgetId], (err, rows) => {
            if (err) {
                reject(err);
            }
            resolve(rows);
        });
    });
};

const getBudgets = async (budgetId, filter, fromTimestamp, toTimestamp, sign) => {
    return new Promise((resolve, reject) => {
        db.all(`SELECT *
                FROM budget_lines
                WHERE budget_id = ?
                  AND (${filter})`, [budgetId], async (err, rows) => {
            if (err) {
                reject(err);
            }
            let totalPlan = 0;
            let totalActual = 0;
            const data = [];
            for (const row of rows) {
                totalPlan += row.amount * sign
                const actualRow = await new Promise((resolve, reject) => {
                    db.get(`SELECT SUM(amount) as amount
                            FROM statements
                            WHERE (${row.fact_filter}) AND time >= ? AND time < ? AND account_id = 'fa_tttbZiiB5EtN6WCtgEw'`, [fromTimestamp, toTimestamp], (err, row) => {
                        if (err) {
                            reject(err);
                        }
                        resolve(row);
                    });
                });
                const actual = actualRow.amount ? Math.round(actualRow.amount / 100) : 0;
                totalActual += actual * sign;
                data.push({
                        name: row.name, plan: row.amount, actual: actual * sign,
                    }
                );
            }
            resolve({rows: data, totalPlan, totalActual});
        });
    });
}

(async () => {
    const {fromTimestamp, toTimestamp} = getMonthRange(m);
    let n = 0;
    n++;
    data.push([n, 'Залишок на початок', 'У валюті', 'Еквівалент', 'Валюта / Курс'])

    const balances = await getBalances(budgetId);
    let totalBalace = 0;

    let nn = 0;
    for (const balance of balances) {
        totalBalace += balance.amount;
        if (!short)
            data.push([n + '.' + ++nn, balance.name, f(balance.currency_amount), f(balance.amount), balance.currency + (balance.currency_rate !== 1 ? ' ' + balance.currency_rate.toFixed(2) : '')])
    }
    data.push([n + '.0', 'Всього залишок', '', f(totalBalace), ''])

    data.push(['', '', '', '', '']);

    n++;
    nn = 0;
    data.push([n, 'Доходи', 'План', 'Факт', 'Різниця']);
    const ins = await getBudgets(budgetId, 'amount > 0', fromTimestamp, toTimestamp, 1);
    if (!short)
        for (const row of ins.rows) {
            data.push([n + '.' + ++nn, row.name, f(row.plan), row.actual ? f(row.actual) : '', (row.actual ? f(row.actual - row.plan) : '')])
        }
    if (ins.rows.length > 1 || short)
        data.push([n + '.0', 'Всього доходів', f(ins.totalPlan), f(ins.totalActual), f(ins.totalActual - ins.totalPlan)]);

    data.push(['', '', '', '', '']);

    n++;
    nn = 0;
    data.push([n, 'Операційні витрати', 'План', 'Факт', 'Різниця']);
    const opOuts = await getBudgets(budgetId, 'amount < 0 AND tag = "op"', fromTimestamp, toTimestamp, -1);
    if (!short)
        for (const out of opOuts.rows) {
            data.push([n + '.' + ++nn, out.name, f(-out.plan), out.actual ? f(out.actual) : '', (out.actual ? f(-out.plan - out.actual) : '')])
        }
    if (opOuts.rows.length > 1 || short)
        data.push([n + '.0', 'Всього операційних витрат', f(opOuts.totalPlan), f(opOuts.totalActual), opOuts.totalActual ? f(opOuts.totalPlan - opOuts.totalActual) : '']);

    data.push(['', '', '', '', '']);

    n++;
    nn = 0;
    data.push([n, 'Особисті витрати', 'План', 'Факт', 'Різниця']);
    const personalOuts = await getBudgets(budgetId, 'amount < 0 AND tag IS NULL', fromTimestamp, toTimestamp, -1);
    if (!short)
        for (const out of personalOuts.rows) {
            data.push([n + '.' + ++nn, out.name, f(-out.plan), out.actual ? f(out.actual) : '', (out.actual ? f(-out.plan - out.actual) : '')])
        }
    if (personalOuts.rows.length > 1 || short)
        data.push([n + '.0', 'Всього особистих витрат', f(personalOuts.totalPlan), f(personalOuts.totalActual), f(personalOuts.totalPlan - personalOuts.totalActual)]);

    data.push(['', '', '', '', '']);

    n++;
    data.push([n, 'Всього витрат', f(opOuts.totalPlan + personalOuts.totalPlan), f(opOuts.totalActual + personalOuts.totalActual), f((opOuts.totalPlan + personalOuts.totalPlan) - (opOuts.totalActual + personalOuts.totalActual))]);

    data.push(['', '', '', '', '']);

    n++;
    data.push([n, 'Баланс', f(ins.totalPlan - (opOuts.totalPlan + personalOuts.totalPlan)), f(ins.totalActual - (opOuts.totalActual + personalOuts.totalActual)), f((ins.totalPlan - (opOuts.totalPlan + personalOuts.totalPlan)) - (ins.totalActual - (opOuts.totalActual + personalOuts.totalActual)))]);

    console.log(table.table(data, {
        columns: [{alignment: 'right'}, {alignment: 'left'}, {alignment: 'right'}, {alignment: 'right'}, {alignment: 'right'}],
        spanningCells: [
            {col: 0, row: 0, colSpan: 5, alignment: 'center'},
        ]
    }));
})();
