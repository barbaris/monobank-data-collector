require('dotenv').config();
const axios = require('axios');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./monobank.sqlite');

const getLastTransaction = () => {
    return new Promise((resolve, reject) => {
        db.get('SELECT * FROM statements ORDER BY time DESC LIMIT 1', (err, row) => {
            if (err) reject(err);
            resolve(row);
        });
    });
}

const insertTransaction = (transaction) => {
    return new Promise((resolve, reject) => {
        db.run('INSERT INTO statements (id, account_id, time, description, amount, operation_amount, cashback_amount, commission_rate, balance, hold, mcc, original_mcc, currency_code) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            [
                transaction.id,
                transaction.accountId,
                transaction.time,
                transaction.description,
                transaction.amount,
                transaction.operationAmount,
                transaction.cashbackAmount,
                transaction.commissionRate,
                transaction.balance,
                transaction.hold,
                transaction.mcc,
                transaction.originalMcc,
                transaction.currencyCode
            ], (err) => {
                if (err) reject(err);
                resolve();
            });
    });
};

const getAccounts = (clientId) => {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM accounts WHERE client_id = ? ORDER BY position', [clientId], (err, rows) => {
            if (err) reject(err);
            resolve(rows);
        });
    });
}

const getClients = () => {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM clients ORDER BY position', (err, rows) => {
            if (err) reject(err);
            resolve(rows);
        });
    });
}

const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

(async () => {
    console.log('Importing bank transactions...');
    const clients = await getClients();
    for (const client of clients) {
        console.log(`Importing transactions for client ${client.name} (${client.id})...`);
        const accounts = await getAccounts(client.id);
        for (const account of accounts) {
            console.log(`Importing transactions for account ${account.description}...`);
            const accessToken = process.env[`MONOBANK_ACCESS_TOKEN_${client.id.toString().toUpperCase()}`] || false;
            if (!accessToken) {
                console.log(`Access token for account ${client.id} not found`);
                continue;
            }
            const lastTransaction = await getLastTransaction();
            const from = lastTransaction.time + 1;
            const now = Math.round((new Date()).getTime() / 1000);
            let to = now;
            if ((now - from) >= 2682000) {
                to = from + 2682000;
            }
            const accountId = account.id
            const transactionsResponse = await axios.get(`https://api.monobank.ua/personal/statement/${accountId}/${from}/${to}`, {
                headers: {
                    'X-Token': accessToken
                }
            });
            const transactions = transactionsResponse.data;
            for (const transaction of transactions) {
                const comment = transaction.comment ? ` (${transaction.comment})` : '';
                transaction.accountId = accountId;
                console.log(`Importing transaction ${transaction.amount / 100} ${transaction.description}${comment}`);
                try {
                    await insertTransaction(transaction);
                } catch (e) {
                    console.log(e);
                }
            }
            await sleep(1000 * 60);
        }
    }
})();
