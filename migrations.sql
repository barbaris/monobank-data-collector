create table clients
(
    id   TEXT not null
        constraint clients_pk
            primary key,
    name TEXT not null
);

create table accounts
(
    id           TEXT              not null
        constraint accounts_pk
            primary key,
    client_id    TEXT              not null,
    last_sync_at INTEGER default 0 not null,
    description  TEXT,
    created_at   INTEGER default 0 not null
);

create table statements
(
    id               TEXT
        constraint statements_pk
            primary key,
    time             INTEGER not null,
    account_id       INTEGER,
    description      TEXT,
    amount           INTEGER not null,
    operation_amount INTEGER not null,
    commission_rate  INTEGER,
    balance          INTEGER,
    hold             INTEGER,
    cashback_amount  INTEGER,
    currency_code    INTEGER,
    mcc              INTEGER,
    original_mcc     INTEGER
);

create unique index statements_id_uindex
    on statements (id);

alter table clients
    add position INTEGER default 0 not null;

alter table accounts
    add position INTEGER default 0 not null;

alter table statements
    add ignore INTEGER default 0 not null;

create table tags
(
    statement_id TEXT not null,
    tag          TEXT
);
